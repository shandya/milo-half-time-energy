<div class="region region-navigation"><div class="navigation--wrapper"><div class="navigation-col">
        <nav role="navigation" aria-labelledby="block-milo-main-menu-menu" id="block-milo-main-menu" data-block-plugin-id="system_menu_block:main" class="block block-menu navigation menu--main">
            <h2 class="sr-only" id="block-milo-main-menu-menu">Main navigation</h2>
      <header>
        <nav class="navbar navbar-expand-lg navbar-light main-navbar">
          <div class="container">
            
      <a class="navbar-brand m-0" href="https://www.milo.co.id/">
          <picture>
              <source srcset="https://www.milo.co.id/themes/custom/milo/assets/images/logo.png" media="(min-width: 600px)">
              <source srcset="https://www.milo.co.id/themes/custom/milo/assets/images/milo-logo-mobile.webp" type="image/webp">
              <img src="https://www.milo.co.id/themes/custom/milo/assets/images/milo-logo-mobile.png" class="img-fluid" alt="MILO Indonesia">
          </picture>
      </a>      <button class="navbar-toggler" type="button" data-toggle="offcanvas" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                <img src="https://www.milo.co.id/themes/custom/milo/assets/images/icons/icon-menu.svg" class="img-fluid" alt="MILO Indonesia" width="28" height="28">
            </button>
      
            <div class="offcanvas-collapse">
              <div class="offcanvas-logo ">
                
      <a class="navbar-brand m-0" href="https://www.milo.co.id/">
          <picture>
              <source srcset="https://www.milo.co.id/themes/custom/milo/assets/images/logo.png" media="(min-width: 600px)">
              <source srcset="https://www.milo.co.id/themes/custom/milo/assets/images/milo-logo-mobile.webp" type="image/webp">
              <img src="https://www.milo.co.id/themes/custom/milo/assets/images/milo-logo-mobile.png" class="img-fluid" alt="MILO Indonesia">
          </picture>
      </a>          <a href="javascript:void(0)" class="offcanvas-close" data-toggle="offcanvas" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <img src="https://www.milo.co.id/themes/custom/milo/assets/images/icons/icon-close.svg" class="img-fluid" alt="MILO Indonesia" width="28" height="28">
                </a>
              </div>
              <div class="navbar-collapse " id="main-nav">
                <ul class="navbar-nav navbar-left">
                                                <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownNutrisimilo">NUTRISI MILO</a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownNutrisimilo">
                        <a class="dropdown-item" href="/produk">PRODUK MILO</a>
                            <a class="dropdown-item" href="/milo-sarapan">MILO SARAPAN</a>
                          </div>
                                        </li>
                                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownActivindonesia">ACTIV INDONESIA</a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownActivindonesia">
                        <a class="dropdown-item" href="/activ-academy">Activ Academy</a>
                            <a class="dropdown-item" href="/activ-indonesia">TENTANG ACTIV INDONESIA</a>
                                            <a class="dropdown-item pl-4" href="/energiuntukraihlebih">Energi Untuk Raih Lebih</a>
                    <a class="dropdown-item pl-4" href="/sarapan-untuk-raih-lebih">Sarapan Untuk Raih Lebih</a>
                
                          </div>
                                              </li>
                                            <li class="nav-item dropdown">
                    <a class="nav-link" href="https://www.milo.co.id/resep">RESEP MILO</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link" href="https://www.milo.co.id/whats-happening	"><span>TERBARU</span> DARI MILO</a>
                  </li>
                                  <li class="user-nav nav-item d-block d-lg-none"><a class="nav-link" href="https://www.milo.co.id/login">Masuk</a></li>
                      <li class="user-nav nav-item d-block d-lg-none"><a class="nav-link" href="https://www.milo.co.id/register">Daftar</a></li>
                            </ul>
                          <div class="offcanvas-footer d-block d-lg-none">
                                <div class="atc">
                    <div class="row align-items-center">
                      <div class="col-6 atc-desc">
                        <figure class="atc-img">
                          <img src="https://www.milo.co.id/themes/custom/milo/assets/images/buy-mini-product-new.webp" data-jpg="https://www.milo.co.id/themes/custom/milo/assets/images/buy-mini-product-new.png" class="img-fluid img-webp" alt="buy now image">
                        </figure>
                        <div class="atc-text">
                          BELI PRODUK MILO
                        </div>
                      </div>
                      <div class="col-6">
                        <button class="btn btn-warning atc-button btn-block lightbox-call-to-action" data-product-id="v3w89l0">
                          Beli Sekarang
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      
            <nav class="user-menu--wrapper d-none d-lg-block">
              <ul class="list-unstyled d-flex align-items-center">
                            <li><a href="https://www.milo.co.id/login">Masuk</a></li>
                  <li><a href="https://www.milo.co.id/register">Daftar</a></li>
                        </ul>
            </nav>
          </div>
        </nav>
      </header>
            </nav>
      </div>
       </div></div>
