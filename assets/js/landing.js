$(window).on("load resize",function(e){
  $('.goodness-list').slick({
    dots: true,
    arrows: false,
    mobileFirst: true,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 768,
        settings: "unslick",
      }
    ]
  });
  
  $('.articles-list').slick({
    dots: false,
    arrows: false,
    mobileFirst: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: "unslick",
      }
    ]
  });
});